const numberButtons = document.querySelectorAll('.data-number');

const operationButtons = document.querySelectorAll('.data-operation');

const equalButton = document.querySelector('.data-equal');

const deleteButton = document.querySelector('.data-delete');

const clearButton = document.querySelector('.data-clear');

const previousOperand = document.querySelector('.previous-operand');

const currentOperand = document.querySelector('.current-operand');

function updateDisplay(previousNumber, operation, currentNumber)
{
    if(operation === undefined){
       previousOperand.innerHTML = "";
       currentOperand.innerHTML = currentNumber;
       return;
    }
    else {
        previousOperand.innerHTML = previousOperand.toString()+' '+operation;
    }
}

function clear ()
{
    previousOperand.innerHTML="";
    currentOperand.innerHTML = "";
}

function appendNumber (number)
{
    let x = currentOperand.innerHTML;
    currentOperand.innerHTML = x.toString()+number.toString();
}


previousOperand.innerHTML = "";
currentOperand.innerHTML = "";




numberButtons.forEach(button => {
button.addEventListener(
    'click',() => {
       appendNumber(button.innerHTML);
    }
)

});

clearButton.addEventListener(
    'click',()=>{
        clear();
    }
);
